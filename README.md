Workshop tasks
================

- Install Microsoft R Open Version 3.4.0 https://mran.microsoft.com/download/
- Install R Studio  https://www.rstudio.com/products/rstudio/download2/
-  Create an account in Bitbucket  https://bitbucket.org/
-  Run the following to install packages in R

```
pkgs <- c(
  "tidyverse", "GGally", "jpeg", "knitr", 
  "microbenchmark", "png", "pryr",
  "caret", "rattle",
  "Rcmdr", "RcmdrPlugin.IPSUR", "RcmdrPlugin.HH",
  "simmer", "simmer.plot", "glpkAPI", 
  "VIM", "markdown", "rmarkdown",
  "fitdistrplus", "desolve", "FME", "rSymPy",
  "installr", "devtools"
)

install.packages(pkgs)
install.packages("nycflight13")
```



Resources
==========

-  R for Data Science by Wickham http://r4ds.had.co.nz/
-  ggplot: Elegant Graphics for data analysis  http://ggplot2.org/book/  Available electronically through Pitt LIbrary
-  Pro Git book  https://git-scm.com/book/en/v2
-  An introduction to Git and how to use it with R Studio https://r-bio.github.io/intro-git-rstudio/
-  Bitbucket https://bitbucket.org/
-  Introduction to Probability and Statistics Using R - IPSUR package
-  Handbook of Statistical Analysis Using R - HPSUR3 package
-  R Commander http://www.rcommander.com/   RCmdr package